package proj.xtremecafe;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import proj.xtremecafe.model.MenuItem;

public class MainActivity extends AppCompatActivity {

  public static final String TAG = "XTREME CAFE";
  LinearLayout containerMain, checkOutView;
  ArrayList<MenuItem> checkOutItems;
  ArrayList<CheckBox> checkBoxes;
  ImageButton btnCheckOut;
  Button btnNfc, btnClear;
  float total = 0;

  TextView tvTotal;
  private NfcAdapter nfcAdapter;
  SharedPreferences sharedpreferences;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    sharedpreferences = getSharedPreferences(TAG, Context.MODE_PRIVATE);

    containerMain = (LinearLayout) findViewById(R.id.container_main);
    checkOutView = (LinearLayout) findViewById(R.id.checkout_list);
    tvTotal = (TextView) findViewById(R.id.tv_total);
    checkOutView.setVisibility(View.GONE);
    btnCheckOut = (ImageButton) findViewById(R.id.btn_checkout);
    btnCheckOut.setOnClickListener(new View.OnClickListener() {
      @Override

      public void onClick(View v) {
        if (checkOutView.getVisibility() == View.GONE) {
          createCheckoutView();
          checkOutView.setVisibility(View.VISIBLE);
          btnNfc.setVisibility(View.VISIBLE);
          btnClear.setVisibility(View.VISIBLE);
          btnCheckOut.setRotation(180.0f);
          if(checkOutItems != null && !checkOutItems.isEmpty() && checkOutItems.size() > 0 ) {
            sendFile((buildString()));
          }
        } else {
          checkOutView.setVisibility(View.GONE);
          btnNfc.setVisibility(View.GONE);
          btnClear.setVisibility(View.GONE);
          btnCheckOut.setRotation(0.0f);
        }
      }
    });

    checkOutItems = new ArrayList<MenuItem>();
    checkBoxes = new ArrayList<CheckBox>();
    readData();
    PackageManager pm = this.getPackageManager();
    // Check whether NFC is available on device
    if (!pm.hasSystemFeature(PackageManager.FEATURE_NFC)) {
      // NFC is not available on the device.
      Toast.makeText(this, "The device does not has NFC hardware.",
        Toast.LENGTH_SHORT).show();
    }
    // Check whether device is running Android 4.1 or higher
    else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
      // Android Beam feature is not supported.
      Toast.makeText(this, "Android Beam is not supported.",
        Toast.LENGTH_SHORT).show();
    }
    else {
      // NFC and Android Beam file transfer is supported.
      Toast.makeText(this, "Android Beam is supported on your device.",
        Toast.LENGTH_SHORT).show();
    }


    btnClear = (Button) findViewById(R.id.btn_reset);
    btnClear.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        resetList();
      }
    });
    btnNfc = (Button) findViewById(R.id.btn_nfc);
    btnNfc.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //resetList();
        //sendFile(encryptMd5(buildString()));
        sendFile((buildString()));
      }
    });

    btnNfc.setVisibility(View.GONE);


    //populate checkBoxes
/*
    for(MenuItem menuItem: checkOutItems) {
      for (CheckBox checkBox : checkBoxes) {
        if (checkBox.getTag() == menuItem.getProdCode()) {
          checkBox.setChecked(true);
        }
      }
    }
*/


    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
  }

  @Override
  public boolean onOptionsItemSelected(android.view.MenuItem item) {
    switch (item.getItemId()) {
      // Respond to the action bar's Up/Home button
      case android.R.id.home:
        NavUtils.navigateUpFromSameTask(this);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private String buildString(){
    StringBuilder orders = new StringBuilder();
    orders.append("*");
    for(int i = 0; i < checkOutItems.size(); i++){//MenuItem menuItem:checkOutItems){
      MenuItem menuItem = checkOutItems.get(i);
      orders.append(menuItem.getProdCode())
        .append(String.valueOf(menuItem.getCount()));
      if(i < checkOutItems.size()-1) {
        orders.append("%");
      }
    }

    return orders.toString();
  }

  private String encryptMd5(String s) {
    try {
      // Create MD5 Hash
      MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
      digest.update(s.getBytes());
      byte messageDigest[] = digest.digest();

      // Create Hex String
      StringBuffer hexString = new StringBuffer();
      for (int i=0; i<messageDigest.length; i++)
        hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
      return hexString.toString();

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return "";
  }

  public void readData(){
    strOrders = sharedpreferences.getString("order",null);

    InputStream inputStream = this.getResources().openRawResource(R.raw.menu);

    InputStreamReader inputreader = new InputStreamReader(inputStream);
    BufferedReader buffreader = new BufferedReader(inputreader);
    String line;
    StringBuilder text = new StringBuilder();
    MenuItem.MenuClass category = null;

    ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
    checkBoxes = new ArrayList<CheckBox>();
    try {
      while (( line = buffreader.readLine()) != null) {

        String code, name, price;
        code = line.split(":")[0];
        name = line.split(":")[1];
        price = line.split(":")[2];

        MenuItem.MenuClass currCategory = MenuItem.getCategory(code.charAt(0));

        if(currCategory != category){
          if(menuItems.size() > 0 ){
            createViews(MenuItem.MenuClass.getString(category), menuItems);
          }
          menuItems = new ArrayList<MenuItem>();
          menuItems.add(new MenuItem(code, name, "",Float.valueOf(price)));
        }else{
          menuItems.add(new MenuItem(code, name, "",Float.valueOf(price)));
        }

        category = currCategory;
        text.append(line);
        text.append('\n');
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e){
      e.printStackTrace();
    }


  }

  String strOrders;
  public void createViews(String type, ArrayList<MenuItem> menuItems){


    View containerMenu = getLayoutInflater().inflate(R.layout.container_pork, null);
    final LinearLayout container = (LinearLayout) containerMenu.findViewById(R.id.container_pork);

    if(type.equals("WINGS")) {
      View view = (View) containerMenu.findViewById(R.id.dotted_line);
      view.setVisibility(View.GONE);
    }

    boolean foundSame = false;
    for(final MenuItem menuItem: menuItems){

      /*foundSame = false;
      if(strOrders != null && !strOrders.isEmpty()){
        //parse order
        String[] orderArr = strOrders.split("/");
        MenuItem.MenuClass categ;
        for(String order: orderArr){
          if(menuItem.getProdCode().equals(order.charAt(0)+""+order.charAt(1))) {
            menuItem.setCount(Integer.parseInt(order.substring(2)));
            addToCheckOut(menuItem);
            foundSame = true;
          }
        }
      }*/





      View view = getLayoutInflater().inflate(R.layout.item_menu, null);
      TextView tvName, tvDesc, tvPrice;
      CheckBox checkBox;

      tvName = (TextView) view.findViewById(R.id.tvName);
      tvDesc = (TextView) view.findViewById(R.id.tvDesc);
      tvPrice = (TextView) view.findViewById(R.id.tvPrice);

      tvName.setText(menuItem.getMenu());
      tvDesc.setText(menuItem.getDesc());
      tvPrice.setText("P "+menuItem.getPrice());

      checkBox = (CheckBox) view.findViewById(R.id.checkBox);
      checkBox.setTag(menuItem.getProdCode());
      checkBox.setChecked(foundSame);
      checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
          if (isChecked) {
            menuItem.setCount(1);
            addToCheckOut(menuItem);
          } else {
            removeFromCheckOut(menuItem);
          }
        }
      });
      checkBoxes.add(checkBox);

      container.addView(view);
    }

    TextView tvType = (TextView)containerMenu.findViewById(R.id.tv_type);
    tvType.setText(type);
    tvType.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (container.getVisibility() == View.VISIBLE) {
          container.setAlpha(1f);
          container.animate().alpha(0f).translationY(-container.getHeight() * 2)
            .setListener(new AnimatorListenerAdapter() {
              @Override
              public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                container.setVisibility(View.GONE);
              }
            });

        } else {

          container.setAlpha(0f);
          container.animate().alpha(1f).translationY(0)
            .setListener(new AnimatorListenerAdapter() {
              @Override
              public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                container.setVisibility(View.VISIBLE);
              }
            });
        }
      }
    });
    container.setVisibility(View.GONE);
    containerMain.addView(containerMenu);
  }

  public void createCheckoutView(){

    final LinearLayout container = (LinearLayout) checkOutView.findViewById(R.id.checkout_list_container);
    container.removeAllViews();

    for(final MenuItem menuItem: checkOutItems){
      final View view = getLayoutInflater().inflate(R.layout.item_checkout, null);
      TextView tvName, tvPrice;
      final EditText etCount;
      ImageButton btnRemove;

      tvName = (TextView) view.findViewById(R.id.tvName);
      tvPrice = (TextView) view.findViewById(R.id.tvPrice);
      etCount = (EditText) view.findViewById(R.id.et_num);
      etCount.setInputType(InputType.TYPE_NULL);
      etCount.setFocusableInTouchMode(false);
      etCount.setText(String.valueOf(menuItem.getCount()));
      btnRemove = (ImageButton) view.findViewById(R.id.btn_delete);
      btnRemove.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          container.removeView(view);
          //removeFromCheckOut(menuItem);

          for(CheckBox checkBox: checkBoxes){
            if(checkBox.getTag() == menuItem.getProdCode()){
              checkBox.setChecked(false);
            }
          }
        }
      });

      tvName.setText(menuItem.getMenu());
      tvPrice.setText("P "+menuItem.getPrice());

      etCount.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          View view = MainActivity.this.getLayoutInflater().inflate(R.layout.dialog_number_picker, null);
          final NumberPicker numPicker = (NumberPicker) view.findViewById(R.id.numPicker);
          numPicker.setMinValue(1);
          numPicker.setMaxValue(100);

          new AlertDialog.Builder(MainActivity.this)
            .setTitle(menuItem.getMenu())
            .setView(view)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                etCount.setText(String.valueOf(numPicker.getValue()));
                menuItem.setCount(numPicker.getValue());
                addToCheckOut(menuItem);
              }
            })
            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
              }
            })
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show();
        }
      });

      container.addView(view);
    }
  }


  public void addToCheckOut(MenuItem menuItem){
    checkOutItems.remove(menuItem);
    checkOutItems.add(menuItem);
    recomputeTotal();
  }

  public void removeFromCheckOut(MenuItem menuItem){
    checkOutItems.remove(menuItem);
    recomputeTotal();
  }

  public void recomputeTotal(){
    float total = 0;
    for(MenuItem menuItem: checkOutItems){
      total += menuItem.getPrice() * menuItem.getCount();
    }
    tvTotal.setText("P " + total);
    this.total = total;

    String orders =  buildString();
    SharedPreferences.Editor editor = sharedpreferences.edit();
    editor.putString("order", orders);
    editor.commit();
  }

  public void sendFile(final String message) {

    nfcAdapter = NfcAdapter.getDefaultAdapter(this);

    // Check whether NFC is enabled on device
    if(!nfcAdapter.isEnabled()){
      // NFC is disabled, show the settings UI
      // to enable NFC
      Toast.makeText(this, "Please enable NFC.", Toast.LENGTH_SHORT).show();
      startActivity(new Intent(Settings.ACTION_NFC_SETTINGS));
    }
    // Check whether Android Beam feature is enabled on device
    else {
      if (!nfcAdapter.isNdefPushEnabled()) {
        // Android Beam is disabled, show the settings UI
        // to enable Android Beam
        Toast.makeText(this, "Please enable Android Beam.",
          Toast.LENGTH_SHORT).show();
        startActivity(new Intent(Settings.ACTION_NFCSHARING_SETTINGS));
      } else {

        Log.d(TAG, "preparing to send message: " + message);
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(this);
        nfc.setNdefPushMessageCallback(new NfcAdapter.CreateNdefMessageCallback() {
          @Override
          public NdefMessage createNdefMessage(NfcEvent event) {
            Log.d(TAG,"sending message: "+message);
            NdefRecord uriRecord = NdefRecord.createUri(Uri.encode(message));
            //resetList();
            //Toast.makeText(MainActivity.this, "Sent order to NFC app.",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Settings.ACTION_NFCSHARING_SETTINGS));
            return new NdefMessage(new NdefRecord[]{uriRecord});
          }
        }, this, this);
      }
    }
  }

  public void showSnackBarToast(Context context, String message){
    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
    /*if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
      // Do something for lollipop and above versions
      Snackbar.make(((Activity)context).getWindow().getDecorView().findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
    } else*/{
      // do something for phones running an SDK before lollipop
      Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
  }

  public void resetList(){
    total = 0;

    runOnUiThread(new Runnable() {
      @Override
      public void run() {

        for(CheckBox checkBox: checkBoxes){
          checkBox.setChecked(false);
        }

      }
    });

    strOrders = null;
    SharedPreferences.Editor editor = sharedpreferences.edit();
    editor.putString("order", null);
    editor.commit();
    checkOutItems.clear();
    recomputeTotal();
    createCheckoutView();
  }

/*  @Override
  public boolean onCreateOptionsMenu( Menu menu ) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate( R.menu.menu, menu );
    return true;
  }

  @Override
  public boolean onOptionsItemSelected( android.view.MenuItem item ) {
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if( id == R.id.action_reset ) {
      resetList();
      return true;
    }

    return super.onOptionsItemSelected( item );
  }*/
}

