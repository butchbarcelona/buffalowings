package proj.xtremecafe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

/**
 * Created by mbarcelona on 4/19/16.
 */
public class MainMenuActivity extends AppCompatActivity implements View.OnClickListener{

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main_menu);

    TextView tvOrder, tvAbout, tvHelp;
    tvOrder = (TextView) findViewById(R.id.tv_order);
    tvAbout = (TextView) findViewById(R.id.tv_about);
    tvHelp = (TextView) findViewById(R.id.tv_help);

    tvOrder.setOnClickListener(this);
    tvAbout.setOnClickListener(this);
    tvHelp.setOnClickListener(this);



  }

  @Override
  public void onClick(View v) {
    switch(v.getId()){
      case R.id.tv_about:
        startActivity(new Intent(MainMenuActivity.this, AboutActivity.class));
        break;
      case R.id.tv_order:
        startActivity(new Intent(MainMenuActivity.this, MainActivity.class));
        break;
      case R.id.tv_help:
        startActivity(new Intent(MainMenuActivity.this, HelpActivity.class));
        break;
    }
  }
}

