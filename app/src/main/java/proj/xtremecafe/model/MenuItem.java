package proj.xtremecafe.model;

/**
 * Created by mbarcelona on 3/5/16.
 */
public class MenuItem {

  public enum MenuClass{
    PORK,
    BEEF,
    CHICKEN,
    SEAFOOD,
    DRINKS,
    DESSERT,
    PASTA,
    WINGS,
    BUFFALO_BUCKET,
    RICE;

    public static String getString(MenuClass menuClass){
      switch(menuClass){
        case PORK:
          return "PORK";
        case BEEF:
          return "BEEF";
        case CHICKEN:
          return "CHICKEN";
        case SEAFOOD:
          return "SEAFOOD";
        case DRINKS:
          return "DRINKS";
        case DESSERT:
          return "DESSERT";
        case PASTA:
          return "PASTA";
        case WINGS:
          return "WINGS";
        case BUFFALO_BUCKET:
          return "BUFFALO BUCKET";
        case RICE:
          return "RICE";
      }
      return null;
    }

  }


  String prodCode, menu, desc;
  float price;
  int count = 0;


  public MenuItem(String prodCode, String menu, String desc, float price){
    this.prodCode = prodCode;
    this.menu = menu;
    this.desc = desc;
    this.price = price;

  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public String getProdCode() {
    return prodCode;
  }

  public void setProdCode(String prodCode) {
    this.prodCode = prodCode;
  }

  public String getMenu() {
    return menu;
  }

  public void setMenu(String menu) {
    this.menu = menu;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public float getPrice() {
    return price;
  }

  public void setPrice(float price) {
    this.price = price;
  }

  public static MenuItem.MenuClass getCategory(char c){
    MenuItem.MenuClass currCategory = null;
    switch(c){
      case 'P': currCategory = MenuItem.MenuClass.PORK; break;
      case 'W': currCategory = MenuItem.MenuClass.WINGS; break;
      case 'C': currCategory = MenuItem.MenuClass.CHICKEN; break;
      case 'B': currCategory = MenuItem.MenuClass.BEEF; break;
      case 'S': currCategory = MenuItem.MenuClass.SEAFOOD; break;
      case 'D': currCategory = MenuItem.MenuClass.DESSERT; break;
      case 'R': currCategory = MenuItem.MenuClass.RICE; break;
      case 'N': currCategory = MenuItem.MenuClass.PASTA; break;
      case 'Q': currCategory = MenuItem.MenuClass.BUFFALO_BUCKET; break;
      case 'Z': currCategory = MenuItem.MenuClass.DRINKS; break;
    }
    return currCategory;
  }
}
