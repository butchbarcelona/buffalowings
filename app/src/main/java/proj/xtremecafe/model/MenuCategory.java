package proj.xtremecafe.model;

import java.util.ArrayList;

/**
 * Created by mbarcelona on 3/6/16.
 */
public class MenuCategory {

  String category;
  ArrayList<MenuItem> menu;

  public MenuCategory(String category, ArrayList<MenuItem> menu ){
    this.category = category;
    this.menu = menu;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public ArrayList<MenuItem> getMenu() {
    return menu;
  }

  public void setMenu(ArrayList<MenuItem> menu) {
    this.menu = menu;
  }



}
